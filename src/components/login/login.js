import React, { useState, useEffect } from "react";
import { Redirect } from "react-router-dom";
import './login.css'
import { connect } from "react-redux";
import { addUser } from "../../store/actions/dataAction";
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import KeyboardVoiceIcon from '@material-ui/icons/KeyboardVoice';
import Icon from '@material-ui/core/Icon';
import SaveIcon from '@material-ui/icons/Save';

const useStyles = makeStyles(theme => ({
  button: {
    margin: theme.spacing(4),
  },
}));

const Login = props => {
  const classes = useStyles();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [auth, setAuth] = useState(false);
  console.log(auth)
  const handleMail = e => {
    setEmail(e.target.value);
  };

  const handlePassword = e => {
    setPassword(e.target.value);
  };

  const handleSubmit = e => {
    e.preventDefault();
    setAuth(true);
    const userData = {
      email,
      password
    };

    props.addUser(userData);
  };

  // return (
  //   <div className="layout">
  //     <form onSubmit={handleSubmit}>
  //       <div>
  //         <input
  //           type="email"
  //           placeholder="Enter your email"
  //           name="email"
  //           onChange={handleMail}
  //         />
  //       </div>
  //       <div>
  //         <input
  //           type="password"
  //           placeholder="Enter your password"
  //           name="password"
  //           onChange={handlePassword}
  //         />
  //       </div>
  //       <input type="submit" />
  //     </form>
  //     {auth && <Redirect to="/users" />}
  //   </div>
  // );

  return (
    <React.Fragment>
      <form onSubmit={handleSubmit}>
        <div>
          <TextField 
              id="standard-search"
              label="Email" 
              type="email" 
              name="email"
              onChange={handleMail}
          />
        </div>
        <div>
          <TextField
            id="standard-password-input"
            label="Password"
            type="password"
            autoComplete="current-password"
            name="password"
            onChange={handlePassword}
          />
       </div>
        {/* <Button
          variant="contained"
          color="primary"
          className={classes.button}
          endIcon={<Icon>send</Icon>}
          type="submit"
        >
          login
        </Button> */}
        <input type="submit" value="Login"/>
      </form>
        {auth && <Redirect to="/users" />}
    </React.Fragment>
  )


};
export default connect(null, { addUser })(Login);
