import React, { useState } from 'react';
import {ApolloClient, InMemoryCache, gql, useQuery} from '@apollo/client';
import MaterialTable from 'material-table';
import RemoveRedEyeIcon from '@material-ui/icons/RemoveRedEye';
import { Redirect } from 'react-router-dom';
import { addCountryInfo } from '../../store/actions/dataAction'
import { connect } from 'react-redux'

// initialize a GraphQL client
export const client = new ApolloClient({
    cache: new InMemoryCache(),
    uri: 'https://countries.trevorblades.com'
  });


const LIST_COUNTRIES = gql`
  {
    countries {
      name
      code
      emoji
      native
      currency
      phone
    }
  }
`;

const CountrySelect = props => {
    
    const {data, loading, error} = useQuery(LIST_COUNTRIES, {client});
    const [showDetails, setShowDetails] = useState(false);
  
    if (loading || error) {
      return <p>{error ? error.message : 'Loading...'}</p>;
    }
    // console.log(data.countries)
    return (
      <div>
          {/* <select value={country} onChange={event => setCountry(event.target.value)}>
            {data.countries.map(country => (
              <option key={country.code} value={country.code}>
                {country.name}
              </option>
            ))}
          </select> */}
          <MaterialTable
          
          options={{
            pageSize: 5,
            pageSizeOptions: [5, 10, 15, 20],
            paginationType: "stepped",
            actionsColumnIndex: -1,
            filtering: true,
          }}
          columns = { [
              {title: 'Country code', field: 'code'},
              {title: 'Name', field: 'name'},
              {title: 'Emoji', field: 'emoji'},
              // {title: 'Emoji', field: 'emojiU'}

          ]}
          data={JSON.parse(JSON.stringify(data.countries))}
          actions={[
            {
              icon: () => <RemoveRedEyeIcon />,
              tooltip: 'Show details',
              onClick: (event, rowData) => {
                console.log(rowData)
                props.addCountryInfo(rowData)
                setShowDetails(true)
              }
            },
          ]}
          editable={{
            onRowUpdate: (newData, oldData) =>
            new Promise(resolve => {
              setTimeout(() => {
                resolve();
              }, 600);
            }),
          }}
          />
          {showDetails && <Redirect to="/users/details"/>}
      </div>
    );
  }

  export default connect(null, { addCountryInfo })(CountrySelect)