import React, { useEffect } from 'react'
import { connect } from "react-redux";
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import InputLabel from '@material-ui/core/InputLabel';
import { fetchDetails, } from '../../store/actions/dataAction'


const useStyles = makeStyles(theme => ({
    root: {
      '& .MuiTextField-root': {
        margin: theme.spacing(1),
        width: '25ch',
        flexGrow: 1,
 
      },
      menuButton: {
        marginRight: theme.spacing(2),
      },
      title: {
        flexGrow: 1,
      },
    },
  }));
const ShowCountry = (props) => {

  useEffect(() => {

    props.fetchDetails()
  })

    const classes = useStyles();
    console.log(props)
    return (
      <div>
            <div className={classes.root}>
        <AppBar position="static">
          <Toolbar>
            
            <Typography variant="h6" className={classes.title}>
              {props.index} {props.code}
            </Typography>
            
          </Toolbar>
        </AppBar>
      </div>
        <form className={classes.root} noValidate autoComplete="off">
             <div>
             <InputLabel htmlFor="outlined-adornment-amount">Name</InputLabel>
             <TextField
                    disabled
                    id="filled-disabled"
                    placeholder="name"
                    value={props.name}
                    variant="filled"
             />
             <InputLabel htmlFor="outlined-adornment-amount">Emoji</InputLabel>
              <TextField
                    disabled
                    id="filled-disabled"
                    placeholder="emoji"
                    value={props.emoji}
                    variant="filled"
             />
             </div>
              <div>
              <InputLabel htmlFor="outlined-adornment-amount">Native</InputLabel>
              <TextField
                    disabled
                    id="filled-disabled"
                    placeholder="native"
                    value={props.native}
                    variant="filled"
             />
             <InputLabel htmlFor="outlined-adornment-amount">Phone</InputLabel>
              <TextField
                    disabled
                    id="filled-disabled"
                    placeholder="phone"
                    value={props.phone}
                    variant="filled"
             />
              </div>
              <InputLabel>Currency</InputLabel>
              <TextField
                    disabled
                    id="filled-disabled"
                    placeholder="currency"
                    value={props.currency}
                    variant="filled"
             />
        </form>
      </div>
    )
}
const mapStateToProps = state => {
    console.log(state)
    return {
        id: state.dataSource.countryDetails.id,
        index: state.dataSource.countryDetails.__typename,
        name: state.dataSource.countryDetails.name,
        emoji: state.dataSource.countryDetails.emoji,
        native: state.dataSource.countryDetails.native,
        currency: state.dataSource.countryDetails.currency,
        phone: state.dataSource.countryDetails.phone,
        code: state.dataSource.countryDetails.code
        
    }
}

export default connect(mapStateToProps, { fetchDetails } )(ShowCountry)
