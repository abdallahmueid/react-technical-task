import React, { useEffect } from "react";
import { connect } from "react-redux";
import Countries from "../countries/countries";
import { makeStyles } from '@material-ui/core/styles';
import { getUser } from '../../store/actions/dataAction'
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles(theme => ({
  root: {
    '& .MuiTextField-root': {
      margin: theme.spacing(1),
      width: '25ch',
      flexGrow: 1,

    },
    menuButton: {
      marginRight: theme.spacing(1),
    },
    title: {
      flexGrow: 1,
    },
  },
}));
const Index = props => {

  useEffect(() => {
    props.getUser()
  })

  const classes = useStyles();

  return (
    <div>
       <div className={classes.root} style={{height: '120px'}}>
        <AppBar position="static">
          <Toolbar>
            
            <Typography variant="h6" className={classes.title}>
              <h3 style={{color: '#60f542'}}>Welcome </h3>
            <div style={{marginTop:'-50px', marginLeft: '130px'}}>
                <p>User-email: {props.email}</p>
                <p>Password: {props.password}</p>
            </div>
            </Typography>
            
          </Toolbar>
        </AppBar>
      </div>
    
      <Countries />
    </div>
  );
};
const mapStateToProps = state => {
    console.log(state)
  return {
    email: state.dataSource.userInfo.email,
    password: state.dataSource.userInfo.password
  };
};
export default connect(mapStateToProps, { getUser })(Index);
