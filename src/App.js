import React from "react";
import "./App.css";
import Login from "./components/login/login";
import Users from "./components/index/index";
import CountryInfo from "./components/countries/updateCountry";
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import { Provider } from "react-redux";

import Store from "./store/configureStore";

function App() {
  return (
    <Provider store={Store}>
      <Router>
        <Switch>
          <Route path="/users/details" component = { CountryInfo } />
          <Route path="/users" component={ Users } />
          <Route exact path="/" component={ Login } />
        </Switch>
      </Router>
    </Provider>
  );
}

export default App;
