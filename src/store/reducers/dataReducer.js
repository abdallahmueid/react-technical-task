import { Login, SHOW_COUNTRY_INFO, FETCH_COUNTRY_DETAILS,FETCH_USER  } from '../actions/types'

const initialState = {
    user: {},
    country: {},
    countryDetails: [],
    userInfo: [],
}
export default function(state = initialState, action) {
    switch (action.type) {
      case Login:
        return {
          ...state,
          user: action.payload
        };
      case SHOW_COUNTRY_INFO:
        return {
          ...state,
          country: action.payload
        }
      case FETCH_COUNTRY_DETAILS:
        return {
          ...state,
          countryDetails: action.payload
        }
      case FETCH_USER:
        return {
          ...state,
          userInfo: action.payload
        }
      default:
        return state;
    }
  }

