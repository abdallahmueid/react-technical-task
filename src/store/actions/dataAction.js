import { Login, SHOW_COUNTRY_INFO, FETCH_COUNTRY_DETAILS, FETCH_USER } from "./types";
import axios from 'axios';


export const addUser = userData => {
  return dispatch => {
    axios.post('http://localhost:3001/comments',userData)
    .then(response => {
      localStorage.setItem("userId",response.data.id)
      dispatch({
        type: Login,
        payload: response.data
      });
    })
  }
};

export const getUser = () => {
  let userId = localStorage.getItem('userId')
  return dispatch => {
    axios.get(`http://localhost:3001/comments/${userId}`)
    .then(response => { console.log(response)
      dispatch({
        type: FETCH_USER,
        payload: response.data
      })
    })
  }
}


export const addCountryInfo = countryData => {
  return dispatch => {
    axios.post('http://localhost:3001/posts',countryData)
    .then(response => { 
      localStorage.setItem('countryId',response.data.id)
        dispatch ({
          type: SHOW_COUNTRY_INFO,
          payload: response.data
        })
    })
    
  };
};



export const fetchDetails = () => {
  let countryId = localStorage.getItem('countryId')
  return dispatch => {
    axios.get(`http://localhost:3001/posts/${countryId}`)
    .then(response => { console.log(response)
        dispatch ({
          type: FETCH_COUNTRY_DETAILS,
          payload: response.data
        })
    })
    
  };
};
